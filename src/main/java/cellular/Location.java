package cellular;
import datastructure.IGrid;

public class Location {
    
    int row;
    int column;

    public Location(int a, int b) {
        this.row = a;
        this.column = b;
    }

    public boolean isOutOfBounds(Location loc, int a, int b, IGrid grid) {
        if ((loc.row + a) < 0 || (loc.row + a) >= grid.numRows()) {
            return true;
        }
        if ((loc.column + b) < 0 || (loc.column + b) >= grid.numColumns()) {
            return true;
        }
        else return false;
    }
    public Location getnewLocation(Location loc, int a, int b) {
        return new Location(loc.row + a, loc.column + b);
    }
}
