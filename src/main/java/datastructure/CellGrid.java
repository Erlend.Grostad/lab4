package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int cols;
    CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.cols = columns;
        grid = new CellState[this.rows][this.cols];
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                grid[row][col] = initialState;
            }
        }      

	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row < 0 || row > numRows()){
            throw new IndexOutOfBoundsException();
        }
        if (column < 0 || column > numColumns()) {
            throw new IndexOutOfBoundsException();
        }
        grid[row][column] = element;      
        
    }

    @Override
    public CellState get(int row, int column) {
        if (row < 0 || row > numRows()){
            throw new IndexOutOfBoundsException();
        }
        if (column < 0 || column > numColumns()) {
            throw new IndexOutOfBoundsException();
        }
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid gridcopy = new CellGrid(this.rows, this.cols, CellState.DEAD);
        for (int row = 0; row < this.rows; row++) {
            for (int col = 0; col < this.cols; col++) {
                gridcopy.grid[row][col] = grid[row][col];
            }
        }
        return gridcopy;
    }
    
}
